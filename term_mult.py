# (5x^2 - 7x + 9) * (3x^2) = 15x^4 -21x^3 + 27x^2
# [5, -7, 9] * [3, 2] = [15, -21, 27, 0, 0]
# (7x^3 + 10x - 2) * (5x) = 35x^4 + 50x^2 - 10x
# [7, 0, 10, -2] * [5, 1] = [35, 0, 50, 10, 0]

def term_mult(poly, term):
    output = []
    for i in range(len(poly)):
        mult = poly[i] * term[0]
        output.append(mult)
    for i in range(term[1]):
        output.append(0)
    return(output)
print(term_mult([2, 0, -3, 4], [2, 1]))
print(term_mult([7, 0, 10, -2], [3, 2]))


