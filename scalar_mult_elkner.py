def scalar_mult(n, p):
    """
      >>> scalar_mult(5, (4,))
      (20,)
      >>> scalar_mult(3, (2, -5, 1, 0))
      (6, -15, 3, 0)
    """
    p = [n * val for val in p]
    return tuple(p)

