def poly(p, var_string='x'):  #x is a string
    equation = ''
    first_pow = len(p) - 1
    for i, coef in enumerate(p): # enumerate means loop over something and have an automatic counter
        power = first_pow - i
        if coef:
            if coef < 0:
                sign, coef = (' - ' if equation else '- '), -coef
            elif coef > 0: # must be true
                sign = (' + ' if equation else '') #if p is 0 then the code will not put any value in its place

            str_coef = '' if coef == 1 and power != 0 else str(coef) 
            if power == 0:
                str_power = '' # if power equals 0 then it will not put a carrot or exponent or x
            elif power == 1:  # if the power is 1 then it will not put a carrot or exponent
                str_power = var_string
            else:
                str_power = var_string + '^' + str(power) # writing exponents

            equation += sign + str_coef + str_power  # putting the equation together
    print(equation)
poly([4, 0, 5, 6])



