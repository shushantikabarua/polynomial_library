def add_poly(p1, p2):
    """
      >>> add_poly([5, 8, 0, 14], [2, -6, 5, -8])
      [7, 2, 5, 6]
      >>> add_poly([-9, 0, 6, -13, 15], [5, 4, -2, 15, -18])
      [-4, 4, 4, 2, -3]
      >>> add_poly([0, 0, 8, 1, 2], [9, 0, -6, 8, 15])
      [9, 0, 2, 9, 17]
    """
    coeff_list = []
    index = len(p1) - 1
    index2 = len(p2) - 1
    while index >= 0:
        sum_term = p1[index] + p2[index2]
        coeff_list.append(sum_term)
        index = index - 1
        index2 = index2 - 1
    return coeff_list[::-1]

if __name__=='__main__':
    import doctest
    doctest.testmod()
