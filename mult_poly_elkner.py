def mult_poly(p1, p2):
    """
      >>> mult_poly((1, 3), (1, 3))
      (1, 6, 9)
      >>> mult_poly((2, 3, 2), (4, 1))
      (8, 14, 11, 2)
      >>> mult_poly((1, 0, 0), (3, 2, 1))
      (3, 2, 1, 0, 0)
      >>> mult_poly((2, 4), (3, 0, 0, 6, -7, 8))
      (6, 12, 0, 12, 10, -12, 32)
    """
    # create an empty tuple for the product 
    prod = () 

    # set p1 to the shorter polynomial
    if len(p1) > len(p2):
        p1, p2 = p2, p1

    degree = len(p1) - 1

    for i, coeff in enumerate(p1):
        term = (coeff, degree - i)
        prod = add_poly(term_mult(term, p2), prod)

    return prod


if __name__ == "__main__":
    import doctest
    doctest.testmod()

