# (4x^2 + 7x - 3) * 3 = 12x^2 + 21x - 9
# [4, 7, -3] * [3] = [12, 21, -9]
# (2x^3 - 3x + 4) * 4 = 8x^3 - 3x + 4
# [2, 0, -3, 4] * [4] = [8, 0, -12, 16]

def scalar_mult(poly, constant):
    output = []
    for i in range(len(poly)):
        mult = poly[i] * constant
        output.append(mult)
    return(output)
scalar_mult([4, 7, -3], 3)

def scalar_mult(poly, constant):
    output = []
    for i in range(len(poly)):
        mult = poly[i] * constant
        output.append(mult)
    return(output)
scalar_mult([2, 0, -3, 4], 4)


