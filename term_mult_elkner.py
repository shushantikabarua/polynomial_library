def term_mult(term, poly):
    """
      >>> term_mult((3, 4), (1, 2, 3, 4))
      (3, 6, 9, 12, 0, 0, 0, 0)
      >>> term_mult((4, 1), (2, 2, 7))
      (8, 8, 28, 0)
    """
    return scalar_mult(term[0], poly) + (term[1] * (0,))


