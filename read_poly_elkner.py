def read_term(term_str):
    """
      >>> read_term('4x^5')
      (4, 5)
      >>> read_term('7x')
      (7, 1)
      >>> read_term('8.5')
      (8.5, 0)
    """
    # handle most common case - terms of degree 2 or higher
    if 'x^' in term_str:
        coeff_str = term_str[:term_str.find('x')]
        coeff = float(coeff_str) if '.' in coeff_str else int(coeff_str)
        degree = int(term_str[term_str.find('^')+1:])
        return (coeff, degree)
    # handle linear case
    if 'x' in term_str:
        coeff = float(term_str[:-1]) if '.' in term_str else int(term_str[:-1])
        return (coeff, 1)
    # handle the constant case
    return (float(term_str) if '.' in term_str else int(term_str), 0)

def read_poly(poly_str):
    """
      >>> read_poly('-2x^3 + 4x + -5')
      (-2, 0, 4, -5)
      >>> read_poly('2x^3 + 3x^2 + 4x + 5')
      (2, 3, 4, 5)
      >>> read_poly('42')
      (42,)
      >>> read_poly('4x^3 + 2')
      (4, 0, 0, 2)
      >>> read_poly('2.2x')
      (2.2, 0)
    """
    # split the polynomial string into a list of term strings
    terms = poly_str.split(' + ')

    # find the degree of the polynomial
    # for polynomials of degree 2 or higher, search for the '^' and slice off
    # the exponent to the right of it
    if '^' in terms[0]:
        degree = int(terms[0][terms[0].find('^')+1:])
    elif 'x' in terms[0]:
        degree = 1                 # it's a linear polynomial
    else:
        degree = 0                 # it's a constant polynomial

    # create a list with places for degree + 1 coefficients
    poly = (degree + 1) * [0]

    # iterate over the terms, setting each coefficient in the poly list
    for term_str in terms:
        coeff, exp = read_term(term_str)
        poly[degree-exp] = coeff

    return tuple(poly)


