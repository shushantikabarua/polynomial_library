def write_poly(poly):
    """
      >>> write_poly((5,))
      '5'
      >>> write_poly((2, 3, 4, 5))
      '2x^3 + 3x^2 + 4x + 5'
      >>> write_poly((3, 0))
      '3x'
      >>> write_poly((-2, 0, 4, -5))
      '-2x^3 + 4x + -5'
      >>> write_poly((1, 1, 1, 1))
      'x^3 + x^2 + x + 1'
    """
    poly_str = ''

    # Handle the 1st and 0th degree terms specially
    # First check for constant
    if len(poly) == 1:
        return str(poly[0])
    # create poly string with linear term
    # if coefficient is 1, don't print it
    coeff = poly[-2] if poly[-2] != 1 else ''
    poly_str = f'{coeff}x'
    # if constant term is not zero, append it
    if poly[-1]:
        poly_str += f' + {poly[-1]}'

    # prepend remaining terms from lowest to highest degree
    # since we are moving from end of list toward front, index of degree
    # term is -(degree+1)
    for degree in range(2, len(poly)):
        # the coefficient is zero, skip it
        if poly[-(degree+1)] == 0:
            continue
        # if the coefficient is 1, don't write it
        coeff = poly[-(degree+1)] if poly[-(degree+1)] != 1 else ''
        # otherwise, prepend next term, moving from end of list toward
        poly_str = f'{coeff}x^{degree} + ' + poly_str

    return poly_str


